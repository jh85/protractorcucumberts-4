import { Before, After, Status, BeforeAll } from "cucumber";
import { browser } from "protractor";
import { assert } from "chai";

Before(async function(): Promise<any> {
    await browser.get('https://www.travelrepublic.ie/');
    assert.equal(await browser.getCurrentUrl(), 'https://www.travelrepublic.ie/', 'Main URL is not loaded');
});

After(async function(scenario): Promise<any> {
    if(scenario.result.status === Status.FAILED) {
        const screenshot = await browser.takeScreenshot();
        this.attach(screenshot, 'image/png');
    }
});