import { ElementArrayFinder, $$ } from "protractor";

export class MainSearchTabPO {
    searchTabs: ElementArrayFinder = $$('.search-tabs > li > a');
}