import { $, ElementFinder, ElementArrayFinder, $$ } from "protractor";

export class FlightHotelPO {
    lblFields: ElementArrayFinder = $$('.field-label');
    tfDestination: ElementFinder = $("input[class*='ng-empty'][ng-blur='ac.blur()'],[class*='ng-touched']");
    lstDestination: ElementArrayFinder = $$(".search-unit-field-row>div:nth-child(1)>div>div:nth-child(2)>ul>li[class='field-options-item']");
    lblDestination: ElementArrayFinder = $$('.field-options-highlight');
    lblDefAirport: ElementFinder = $('.more-text>span:nth-child(1)');
    cbbDate: ElementFinder = $('.content-placeholder');
    cbbOccupant: ElementFinder = $("input[class='ng-pristine ng-untouched ng-valid ng-not-empty']");
    ckbOpts: ElementArrayFinder = $$("input[class*='sc-o-checkbox__input']");
    cbbOpts: ElementArrayFinder = $$('.select-box-text');
    tfTest = $('.ng-valid.ng-dirty.ng-touched.ng-not-empty')
}