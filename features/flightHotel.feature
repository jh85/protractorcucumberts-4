Feature: flightHotel

   Feature Description Test Flight+Hotel main search tab features

   Scenario: Validate Flight+Hotel tab in default state
      When Validate Flight+Hotel tab is selected
      Then Validate default state of its properties

   Scenario Outline: Select '<ckb>' checkbox
      Given '<ckb>' is not selected
      When Click checkbox
      Then '<ckb>' is selected

      Examples:
         | ckb         |
         | flexible    |
         | direct only |

   Scenario Outline: Validate destination auto-suggest contains / equals '<loc>'

      Given Enter '<loc>'
      When Validate text field equals '<loc>'
      Then Validate auto-suggest includes / equals '<loc>'
      And Clear field

      Examples:
         | loc          |
         | Cyberjaya    |
         | Kuala Lumpur |
