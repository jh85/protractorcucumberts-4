import { When, Then, Given } from "cucumber";
import { MainSearchTabPO } from "../page_objects/mainSearchTabPO";
import { FlightHotelPO } from "../page_objects/flightHotelPO";
import { assert } from "chai";
import { browser, ElementArrayFinder } from "protractor";

const mainSearchTabPO = new MainSearchTabPO();
const flightHotelPO = new FlightHotelPO();
let actual: any, expect: any, index: number;

When('Validate Flight+Hotel tab is selected', async (): Promise<any> => {
    actual = await flightHotelPO.lblFields.get(0).getText().then().catch(() => false);
    expect = 'Where to?';
    assert.equal(actual, expect, 'Current tab is not Flight+Hotel');
});

Then('Validate default state of its properties', async (): Promise<any> => {
    actual = await flightHotelPO.tfDestination.getAttribute('placeholder');
    expect = 'Enter a destination or hotel name';
    assert.equal(actual, expect, 'Destination default placeholder');

    actual = await flightHotelPO.lblDefAirport.getText();
    expect = 'Dublin';
    assert.equal(actual, expect, 'Default airport');

    actual = await flightHotelPO.cbbDate.getText();
    expect = 'Select your date range';
    assert.equal(actual, expect, 'Default text for date');

    // actual = await flightHotelPO.ckbOpts.get(0).isSelected();
    // assert.isTrue(actual, 'Flexible checkbox');

    // actual = await flightHotelPO.ckbOpts.get(1).isSelected();
    // assert.isFalse(actual, 'Direct Only');

    actual = await flightHotelPO.cbbOpts.get(0).getText();
    expect = 'Economy';
    assert.equal(actual, expect, 'Cabin class');

    actual = await flightHotelPO.cbbOpts.get(1).getText();
    expect = 'All airlines';
    assert.equal(actual, expect, 'Airline');
});

Given('{string} is not selected', async (string: string): Promise<any> => {
    // string === 'flexible' ? index = 0 : index = 1;
    // actual = await flightHotelPO.ckbOpts.get(index).isSelected();
});

When('Click checkbox', async (): Promise<any> => {
    // if (actual === false) await browser.executeScript('arguments[0].click();',
    //     flightHotelPO.ckbOpts.get(index));
});

Then('{string} is selected', async (string: string): Promise<any> => {
    // actual = await flightHotelPO.ckbOpts.get(index).isSelected();
    // assert.isTrue(actual, `${string} is not selected`);
});

Given('Enter {string}', async (string): Promise<any> => await flightHotelPO.tfDestination.sendKeys(string));

When('Validate text field equals {string}', async (string): Promise<any> => {
    // actual = await flightHotelPO.tfDestination.getAttribute('value');
    // expect = string;
    // assert.equal(actual, expect, 'Destination text field');
})

Then('Validate auto-suggest includes / equals {string}', async (string): Promise<any> => {
    actual = await flightHotelPO.lblDestination.reduce((acc: boolean, elm: ElementArrayFinder) => {
        if(acc === false) return acc;
        return elm.getText().then(text => {
            if(text.localeCompare(string, undefined, { sensitivity: 'base' }) !== 0) return false;
        });
    }, true);
    assert.isTrue(actual, 'Destination auto-suggest list');
});
Then('Clear field', async (): Promise<any> => {
    // await flightHotelPO.lstDestination.get(0).click();
    // await flightHotelPO.tfDestination.click();
    // await flightHotelPO.tfTest.clear();
    // actual = await flightHotelPO.lblDestination.getAttribute('value');
    // assert.equal(actual, '', 'Clear destination field');
    await browser.sleep(5000);
});

