"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const mainSearchTabPO_1 = require("../page_objects/mainSearchTabPO");
const flightHotelPO_1 = require("../page_objects/flightHotelPO");
const chai_1 = require("chai");
const protractor_1 = require("protractor");
const mainSearchTabPO = new mainSearchTabPO_1.MainSearchTabPO();
const flightHotelPO = new flightHotelPO_1.FlightHotelPO();
let actual, expect, index;
cucumber_1.When('Validate Flight+Hotel tab is selected', () => __awaiter(void 0, void 0, void 0, function* () {
    actual = yield flightHotelPO.lblFields.get(0).getText().then().catch(() => false);
    expect = 'Where to?';
    chai_1.assert.equal(actual, expect, 'Current tab is not Flight+Hotel');
}));
cucumber_1.Then('Validate default state of its properties', () => __awaiter(void 0, void 0, void 0, function* () {
    actual = yield flightHotelPO.tfDestination.getAttribute('placeholder');
    expect = 'Enter a destination or hotel name';
    chai_1.assert.equal(actual, expect, 'Destination default placeholder');
    actual = yield flightHotelPO.lblDefAirport.getText();
    expect = 'Dublin';
    chai_1.assert.equal(actual, expect, 'Default airport');
    actual = yield flightHotelPO.cbbDate.getText();
    expect = 'Select your date range';
    chai_1.assert.equal(actual, expect, 'Default text for date');
    // actual = await flightHotelPO.ckbOpts.get(0).isSelected();
    // assert.isTrue(actual, 'Flexible checkbox');
    // actual = await flightHotelPO.ckbOpts.get(1).isSelected();
    // assert.isFalse(actual, 'Direct Only');
    actual = yield flightHotelPO.cbbOpts.get(0).getText();
    expect = 'Economy';
    chai_1.assert.equal(actual, expect, 'Cabin class');
    actual = yield flightHotelPO.cbbOpts.get(1).getText();
    expect = 'All airlines';
    chai_1.assert.equal(actual, expect, 'Airline');
}));
cucumber_1.Given('{string} is not selected', (string) => __awaiter(void 0, void 0, void 0, function* () {
    // string === 'flexible' ? index = 0 : index = 1;
    // actual = await flightHotelPO.ckbOpts.get(index).isSelected();
}));
cucumber_1.When('Click checkbox', () => __awaiter(void 0, void 0, void 0, function* () {
    // if (actual === false) await browser.executeScript('arguments[0].click();',
    //     flightHotelPO.ckbOpts.get(index));
}));
cucumber_1.Then('{string} is selected', (string) => __awaiter(void 0, void 0, void 0, function* () {
    // actual = await flightHotelPO.ckbOpts.get(index).isSelected();
    // assert.isTrue(actual, `${string} is not selected`);
}));
cucumber_1.Given('Enter {string}', (string) => __awaiter(void 0, void 0, void 0, function* () { return yield flightHotelPO.tfDestination.sendKeys(string); }));
cucumber_1.When('Validate text field equals {string}', (string) => __awaiter(void 0, void 0, void 0, function* () {
    // actual = await flightHotelPO.tfDestination.getAttribute('value');
    // expect = string;
    // assert.equal(actual, expect, 'Destination text field');
}));
cucumber_1.Then('Validate auto-suggest includes / equals {string}', (string) => __awaiter(void 0, void 0, void 0, function* () {
    actual = yield flightHotelPO.lblDestination.reduce((acc, elm) => {
        if (acc === false)
            return acc;
        return elm.getText().then(text => {
            if (text.localeCompare(string, undefined, { sensitivity: 'base' }) !== 0)
                return false;
        });
    }, true);
    chai_1.assert.isTrue(actual, 'Destination auto-suggest list');
}));
cucumber_1.Then('Clear field', () => __awaiter(void 0, void 0, void 0, function* () {
    // await flightHotelPO.lstDestination.get(0).click();
    // await flightHotelPO.tfDestination.click();
    // await flightHotelPO.tfTest.clear();
    // actual = await flightHotelPO.lblDestination.getAttribute('value');
    // assert.equal(actual, '', 'Clear destination field');
    yield protractor_1.browser.sleep(5000);
}));
