"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainSearchTabPO = void 0;
const protractor_1 = require("protractor");
class MainSearchTabPO {
    constructor() {
        this.searchTabs = protractor_1.$$('.search-tabs > li > a');
    }
}
exports.MainSearchTabPO = MainSearchTabPO;
