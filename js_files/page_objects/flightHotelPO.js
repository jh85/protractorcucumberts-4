"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FlightHotelPO = void 0;
const protractor_1 = require("protractor");
class FlightHotelPO {
    constructor() {
        this.lblFields = protractor_1.$$('.field-label');
        this.tfDestination = protractor_1.$("input[class*='ng-empty'][ng-blur='ac.blur()'],[class*='ng-touched']");
        this.lstDestination = protractor_1.$$(".search-unit-field-row>div:nth-child(1)>div>div:nth-child(2)>ul>li[class='field-options-item']");
        this.lblDestination = protractor_1.$$('.field-options-highlight');
        this.lblDefAirport = protractor_1.$('.more-text>span:nth-child(1)');
        this.cbbDate = protractor_1.$('.content-placeholder');
        this.cbbOccupant = protractor_1.$("input[class='ng-pristine ng-untouched ng-valid ng-not-empty']");
        this.ckbOpts = protractor_1.$$("input[class*='sc-o-checkbox__input']");
        this.cbbOpts = protractor_1.$$('.select-box-text');
        this.tfTest = protractor_1.$('.ng-valid.ng-dirty.ng-touched.ng-not-empty');
    }
}
exports.FlightHotelPO = FlightHotelPO;
